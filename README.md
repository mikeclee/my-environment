My Environment Information
==========================

**Note:** These are notes on my local environment setups.  Please compare the hardware context and make necessary adjustments.  Absolutely no warranty on any issue with usage!

## Neovim

This is a track and log of my local neovim setup in [.config/nvim](./.config/nvim)

### Setup

Official [neovim installation](https://github.com/neovim/neovim/wiki/Installing-Neovim) guide

### Requirements

- [Neovim](https://neovim.io/) (version 0.8 or later)
- [Nerd Font](https://www.nerdfonts.com/) [Optional] for better icon and font display

#### MacOS

- [XCode Command Line Tools](https://developer.apple.com/xcode/resources/) MacOS developer tools
```bash
xcode-select --install
```
- [Homebrew](https://brew.sh/) [Optional] easier package management
```bash
bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```
- [iTerm2](https://iterm2.com/) [Optional] popular true color terminal
```bash
brew install --cask iterm2
```
- [Ripgrep](https://github.com/BurntSushi/ripgrep) - For Telescope Fuzzy Finder
```bash
brew install ripgrep
```
- quick Neovim install
```bash
brew install neovim
```
